---
layout: article
title: ¡Ya tenemos web!
modified:
categories: blog
excerpt: La OfiLibre, por fin, publica su página web, donde podrás encontrar gran cantidad de resursos libres.
tags: []
image:
  feature: /blog/anuncio-web/logo-ofilibre-blanco.jpg
  teaser: /blog/anuncio-web/logo-ofilibre.png
  thumb: /blog/anuncio-web/logo-ofilibre.png
---


La OfiLibre lleva ocho meses puesta en marcha y, por fin, anunciamos nuestra página web. En esta web podréis encontrar recursos libres de todo tipo (guías de programas libres, imágenes, sonidos...). Además, publicaremos noticias sobre el mundo del software, las licencias, publicaciones... Para que tengáis la mayor información posible a la hora de publicar o elegir medios.

También, usaremos este medio para que nos hagáis propuestas de eventos sobre temas relacionados con la OfiLibre, o peticiones de guías o instrucciones de cualquier programa de licencia libre. Actualmente contamos con un blog en el que podréis leer noticias, informaros de eventos próximos y mucho más, también disponéis de una serie de fichas y guías de programas libres para que aprendáis a usarlos y sepáis las diferentes alternativas que existen.

Inauguramos, así, nuestra página web.

**¡Esperamos vuestras propuestas y visitas!**