---
layout: archive
permalink: /fichas/
title: "Fichas de programas y de recursos"
image:
  feature: logo-ofilibre-blanco.jpg
---


<div class="tiles">
{% for ficha in site.fichas %}
	{% include ficha-grid.html %}
{% endfor %}
</div><!-- /.tiles -->

