---
title: Definición de obra abierta (mini-presentación)
date: 2019-05-01
image:
  feature: Definiciones.png
  teaser: Definiciones.png
transpas:
  - format: PDF
    file: Definiciones.pdf
  - format: ODP (LibreOffice)
    file: Definiciones.odp
---

Definiciones de conocimiento abierto, obra cultural libre y software libre
(mini-presentación, sólo incluye enlaces a las definiciones más comúnmente aceptadas).

