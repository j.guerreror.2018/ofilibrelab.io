---
title: Datos abiertos
date: 2019-04-08
image:
  feature: Datos_Abiertos.png
  teaser: Datos_Abiertos.png
transpas:
  - format: PDF
    file: Datos_Abiertos.pdf
  - format: ODP (LibreOffice)
    file: Datos_Abiertos.odp
extras:
  - name: Presentación en el Campus de Móstoles
    url: https://eventos.urjc.es/34110/detail/cafe-y-datos-abiertos.html
---

Cada vez más, los datos abiertos se están convirtiendo en un requsito en muchos campos de investigación, en un síntoma de transparencia, y en un recurso muy útil en docencia e investigación. ¿Qué ventajas tienen? ¿Qué problemas y desafíos plantean? ¿Dónde puedo encontrar datos abiertos? ¿Cómo puedo hacer que los datos que produzco sean abiertos? Durante este rato, trataremos de abordar estas y otras preguntas, e iniciaremos un  debate sobre la relación que queremos tener con los datos abiertos como docentes e investigadores.
