---
title: Presentación de la OfiLibre
date: 2019-04-01
image:
  feature: Ofilibre-presentacion.png
  teaser: Ofilibre-presentacion.png
transpas:
  - format: PDF
    file: Ofilibre-presentacion.pdf
  - format: ODP (LibreOffice)
    file: Ofilibre-presentacion.odp
---

Presentación de la Ofilibre, realizada en varios campus
