---
layout: archive
permalink: /contacto
title: "Contactar con la OfiLibre"
image:
  feature: logo-ofilibre-blanco.jpg
---

Oficina de Conocimiento y Cultura Libres.

Universidad Rey Juan Carlos, Campus de Móstoles.

Despacho 004, planta baja edificio Rectorado.

C/ Tulipán s/n, 28933 Móstoles (Madrid)

Correo electrónico: [ofilibre@urjc.es](mailto:ofilibre@urjc.es)

Página web oficial: [urjc.es/ofilibre](https://urjc.es/ofilibre)

Tel. 91 488 4679
